diplomer
===================

**diplomer** - приложение для работы с issue из GitLab. Программа формирует удобный для парсинга JSON файл, а также проверяет issue на правильность заполнения. 

Для того, чтобы разрешить diplomer доступ к репозитортю, в котором лежат issue, нужно в настройках git аккаунта сгенерировать **Access Token**. Данный token должен быть сгенерирован от лица пользователя, issue которого нужно получить. Также нужно знать логин и имя репозитория.

Из дополнительных параметров: теги, состояние issue и имя перподавателей (если нужен поиск issue по данным критериям).
Все параметры вводятся в соотвествующие поля в URL браузера.



#### **Как  сгенерировать Access Token**:

1. В правом верхнем углу нажимаем на иконку пользователя

2. Заходим в settings

3. Заходим во вкладку Access Tokens

4. Добавляем новый Access Token

5. В поле name пишем diplomerREST

6. В поле Expires at ничего не меняем

7. В поле scopes выбираем все пункты

8. Нажимаем кнопку Create personal access token

9. Сохраняем полученный token



#### Как запустить приложение:

`git clone git@gitwork.ru:zomk/diplomer.git`

`cd diplomer/`

`docker build -t diplomer:1.0 -f Dockerfile .`

`docker run -d -p 8000:8000 diplomer:1.0`



#### Точки входа:

1. /issue

   Данная точка входа выводит **только корректно заполненные** issue.

   ##### Обязательные параметры:

   `token` - сгенерированный Access Token

   `state` - состояние issue (all, opened, closed)

   Эти параметры **нельзя** оставлять пустыми!

   

   Вот пример ввода этих параметров в командную строку.

   **Пример:** http://127.0.0.1:8000/diplomer/api/v1/issues/issue/?token=vQHqMeqB_4UoTy3iX_mx&labels=2019-2020&state=all&teachers_name=

   где token - персональный token

   ##### Необязательные параметры:

   `labels`- теги по которым issue

   `teachers_name`- фамилия преподавателей

   Эти параметры **должны** присутствовать в вашем URL, но их **можно** оставлять пустыми(т.е ничего не писать после "=")

   

   Вот пример как получить все issue c тегом 2019-2020 у преподавателя Петорв и открытым состоянием.

   **Пример**: http://127.0.0.1:8000/diplomer/api/v1/issues/issue/?token=vQHqMeqB_4UoTy3iX_mx&labels=2019-2020&state=opened&teachers_name=Петров

   

2. /check_correct

   Данная точка входа выводит **только некорректно заполненные** issue.
   Параметры те же самые , что и у /issue.

   

   Вот пример как получить список всех неккоректно заполненных issue.

   **Пример**: http://127.0.0.1:8000/diplomer/api/v1/issues/check_correct/?token=uMD7JkodRxLRgzKo5vnQ&labels=&state=all&teachers_name=

   

3. С помощью curl

   **Пример:** `curl 'http://127.0.0.1:8000/diplomer/api/v1/issues/issue/?token=vQHqMeqB_4UoTy3iX_mx&login=dull69&project_name=diplomer&labels=2019-2020&state=all&teachers_name=Петров'`

   

#### Как запустить тесты:

`cd /diplomer/diplomerREST`

`pytest`


# Удаление лишних issues
```bash
./del_issues <token>
```
