# pylint: disable=C0301,E1101,E0012,W0613,C1801,R0903,R0914
"""Contain web-functions for getting and showing issues from GitLab"""
import requests
from rest_framework import viewsets
from rest_framework.response import Response
from django.utils.datastructures import MultiValueDictKeyError
from .validation_module import ValidationModule

GLOBAL_TABS = 2
GLOBAL_ERROR_RESPONSE = 400
GLOBAL_OK_RESPONSE = 200


class ValidIssue(viewsets.ViewSet):
    """Show valid issues"""

    @classmethod
    def make_issue_id_list(cls, issues_from_response):
        """Get request and make issue id list
            Args:
                issues_from_response
            Returns:
                issue id list
        """
        issue_id = []
        for elem in issues_from_response:
            issue_id.append(elem['iid'])
        return issue_id

    def split_issue_to_correct(self, issues_from_response):
        """Get request, check all issue and return only correct issue
            Args:
                issues_from_response
            Returns:
                correct issue list
        """
        check_valid = ValidationModule()
        check_valid_response = check_valid.check_json_valid(issues_from_response)
        issue_id = self.make_issue_id_list(issues_from_response)

        if len(check_valid_response['incorrect_issues_id']) != 0:
            correct_issue = []
            correct_issue_id = set(issue_id) - set(check_valid_response['incorrect_issues_id'])
            for cor_iid in correct_issue_id:
                for issue_elem in issues_from_response:
                    if cor_iid == issue_elem['iid']:
                        correct_issue.append(issue_elem)

            return correct_issue
        return issues_from_response

    def list(self, request):
        """Get request from url and return ONLY correct, in edited format issues
            Args:
                request
            Returns:
                If response is correct return issues in json
                Otherwise return response with error
        """
        issue_response = IssueResponse(request)
        issue_response_get = issue_response.get_issues()

        if isinstance(issue_response_get, list):
            issue_teachers_name = issue_response.get_param_from_request()
            correct_issue = self.split_issue_to_correct(issue_response_get)
            if issue_teachers_name['teachers_name'] == "":
                issue_json = IssueFinalJson()
                return Response(issue_json.make_correct_json(correct_issue))

            issue_json_teachers = TeachersJson(issue_teachers_name['teachers_name'])
            return Response(issue_json_teachers.make_correct_json_by_teachers(correct_issue))

        if isinstance(issue_response_get, str):
            return Response(issue_response_get)
        bad_answer = "Error! Connection failed. Check url parameters"
        return Response(bad_answer)


class InvalidIssue(viewsets.ViewSet):
    """Show invalid issues"""

    @classmethod
    def list(cls, request):
        """Get request from url and return ONLY incorrect issues with
        id, error and description
            Args:
                request
            Returns:
                If there are incorrect issues return json
                Otherwise return "good_answer"
        """
        issue_response = IssueResponse(request)
        issue_response_get = issue_response.get_issues()
        issue_teachers_name = issue_response.get_param_from_request()
        if isinstance(issue_response_get, list):
            check_valid = ValidationModule()

            if issue_teachers_name['teachers_name'] == "":
                check_valid_response = check_valid.check_json_valid(issue_response_get)
            else:
                issue_json_teachers = TeachersJson(issue_teachers_name['teachers_name'])
                check_valid_response = check_valid.check_json_valid(issue_json_teachers.find_issue_by_teachers(issue_response_get))

            if len(check_valid_response['incorrect_issues']) != 0:
                return Response(check_valid_response['incorrect_issues'])

            good_answer = "All issues are correct!"
            return Response(good_answer)
        if isinstance(issue_response_get, str):
            return Response(issue_response_get)
        bad_answer = "Error! Connection failed. Check url parameters"
        return Response(bad_answer)


class IssueResponse:
    """This class is getting issue(in json format) from GitLab"""
    def __init__(self, request_from_url):
        """Input args in class object"""
        self._request_from_url = request_from_url

    def get_param_from_request(self):
        """Parse request and get token, login, project name, labels,
        state, teachers name
            Args:
                request from __init__
            Returns:
                dict with parameters from url
        """
        try:
            token = self._request_from_url.GET['token']
            login = ''
                #self._request_from_url.GET['login']
            project_name = ''
                #self._request_from_url.GET['project_name']
            labels = self._request_from_url.GET['labels']
            state = self._request_from_url.GET['state']
            teachers_name = self._request_from_url.GET['teachers_name']

            request_args = {'token': token, 'login': login, 'project_name': project_name,
                            'labels': labels, 'state': state, 'teachers_name': teachers_name,
                            'necessary_param': [token, login, project_name]}
        except MultiValueDictKeyError as exc:
            er = str(exc) + " not found"
            return er

        for req_elem in request_args['necessary_param']:
            if req_elem == "":
                content = "Enter " + str(req_elem)
                request_args['necessary_param'].append(content)
                return request_args
        return request_args

    def get_issues(self):
        """Form the url and get issue json through GitLabAPI
            Args:
                call get_param_request() and get all parameters from there
            Returns:
                If connected to GitLab return json
                Otherwise return error
        """

        param_from_req = self.get_param_from_request()
        if isinstance(param_from_req, dict) and len(param_from_req['necessary_param']) != 0:
            headers = {
                'PRIVATE-TOKEN': param_from_req['token'],
            }
            gitlab_api_path = 'https://gitwork.ru/api/v4/projects/3778' #TODO исправить захардкоженность проекта
            login_and_project_path =''
                #param_from_req['login'] + '%2F' + param_from_req['project_name']
            per_page = '1000' # TODO: костыль, чтобы получать все ишью, а не 20  умолчанию
            labels_path = '/issues?labels='
            issue_path = '/issues'
            state_path = '&state='
            if len(param_from_req['labels']) != 0:
                url = gitlab_api_path + login_and_project_path + labels_path + param_from_req['labels'] + '&per_page=' + per_page + state_path + param_from_req['state']
            else:
                url = gitlab_api_path + login_and_project_path + issue_path + "?state=" + param_from_req['state'] + '&per_page=' + per_page

            response_from_gitlab = requests.get(url, headers=headers)
            if response_from_gitlab.status_code == GLOBAL_OK_RESPONSE:
                return response_from_gitlab.json()

        return param_from_req


class IssueFinalJson:
    """This class contain methods to form correct issue"""
    @classmethod
    def parse_json_from_response(cls, json_response):
        """Parse json from response
            Args:
                json from response
            Returns:
                Parsed json with necessary fields
        """
        msid = 0
        issue_parsed_from_response = []

        for body in json_response:
            msid += 1
            issues_dict = {}
            issues_dict['msid'] = msid
            issues_dict['iid'] = body['iid']
            issues_dict['teachers'] = body['title'][:body['title'].find(':')]
            issues_dict['theme'] = body['title'][body['title'].find(':') + GLOBAL_TABS:]
            issues_dict['title'] = body['title']
            issues_dict['description'] = body['description']
            issue_parsed_from_response.append(issues_dict)

        return issue_parsed_from_response

    def make_correct_json(self, issue_json):
        """Parse issue description and make them to correct form
             Args:
                 Parsed json from parse_json_from_response func
             Returns:
                 Final json with correct form
         """
        issue_parsed_from_response = self.parse_json_from_response(issue_json)
        main_questions = 'Основные вопросы и задачи'
        good_separator = '---'
        for issues_list_elem in issue_parsed_from_response:
            issues_description = issues_list_elem['description']

            issues_description_list = issues_description.split(good_separator)

            parsed_issues_dict = {}

            for list_elem in issues_description_list[:-1]:
                issues_replaced_list = list_elem.replace('**', '').strip("\n").strip("\n")
                if issues_replaced_list.startswith(main_questions):
                    parsed_issues_dict.update({issues_replaced_list[:issues_replaced_list.find(':')]: issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_TABS:].split('\n')})

                    if parsed_issues_dict[issues_replaced_list[:issues_replaced_list.find(':')]][0] == "":
                        parsed_issues_dict[issues_replaced_list[:issues_replaced_list.find(':')]].remove("")

                else:
                    parsed_issues_dict.update({issues_replaced_list[:issues_replaced_list.find(':')]: issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_TABS:]})

            issues_list_elem['description'] = parsed_issues_dict

        #return json.dumps(issue_parsed_from_response, ensure_ascii=False)
        return issue_parsed_from_response


class TeachersJson:
    """This class for finding issues by teachers
        in __init__ (teachers and object from IssueFilnalJson class for composition)
    """
    def __init__(self, teachers=""):
        """Input args in class object"""
        self.teachers = teachers
        self.issue_json = IssueFinalJson()

    def find_issue_by_teachers(self, issue_json):
        """Find issue by teachers
             Args:
                 Response issue from response
             Returns:
                 Json with issue finding by teachers
         """
        splitted_teachers_name = self.teachers.split(',')
        teachers_name_list = []
        issues_dict = {}
        for body in issue_json:
            issues_dict['teachers'] = body['title'][:body['title'].find(':')]
            if issues_dict['teachers'] in splitted_teachers_name:
                teachers_name_list.append(body)
        return teachers_name_list

    def make_correct_json_by_teachers(self, issue_json):
        """Make correct issue json finding by teachers
             Args:
                 Response issue from response
             Returns:
                 Json with correct issue finding by teachers
         """
        teachers_json = self.find_issue_by_teachers(issue_json)
        return self.issue_json.make_correct_json(teachers_json)
