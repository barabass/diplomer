# pylint: disable=C0301
"""Test issue to correct paragraphs"""


def test_issue_to_number_of_paragraphs_autumn_kurs_incorrect_input(supply_json_to_test_num_of_paragraph_kurs_autumn_incorrect_input):
    """Test issue to correct paragraph in kurs autumn with incorrect input"""
    answer = supply_json_to_test_num_of_paragraph_kurs_autumn_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == \
           "Not enough paragrpah in issue description: Основные вопросы и задачи, подлежащие разработке при выполнении курсовой " \
           "работы осеннего семестра in issue: иванов: модель кактуса You should have at least 2"
    assert answer['bad_issues'][0]['id'] == 3


def test_issue_to_number_of_paragraphs_autumn_kurs_incorrect_and_correct_input(supply_json_to_test_num_of_paragraph_kurs_autumn_incorrect_and_correct_input):
    """Test issue to correct paragraph in kurs autumn with incorrect and correct input"""
    answer = supply_json_to_test_num_of_paragraph_kurs_autumn_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == \
           "Not enough paragrpah in issue description: Основные вопросы и задачи, подлежащие разработке при выполнении курсовой " \
           "работы осеннего семестра in issue: Петров: модель земли под коралловым рифом You should have at least 2"
    assert answer['bad_issues'][0]['id'] == 2
    assert str(answer['bad_issues'][1]['error']) == \
           "Not enough paragrpah in issue description: Основные вопросы и задачи, подлежащие разработке при выполнении курсовой " \
           "работы осеннего семестра in issue: Пв: земли под коралловым рифом You should have at least 2"
    assert answer['bad_issues'][1]['id'] == 3


def test_issue_to_number_of_paragraphs_autumn_kurs_correct_input(supply_json_to_test_num_of_paragraph_kurs_autumn_correct_input):
    """Test issue to correct paragraph in kurs autumn with correct input"""
    answer = supply_json_to_test_num_of_paragraph_kurs_autumn_correct_input
    assert answer['bad_issues'] == []


def test_issue_to_number_of_paragraphs_spring_kurs_incorrect_input(supply_json_to_test_num_of_paragraph_kurs_spring_incorrect_input):
    """Test issue to correct paragraphs spring kurs with incorrect input"""
    answer = supply_json_to_test_num_of_paragraph_kurs_spring_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == \
           "Not enough paragrpah in issue description: Основные вопросы и задачи, подлежащие разработке при выполнении курсовой " \
           "работы весеннего семестра in issue: Петров: модель земли под коралловым рифом You should have at least 2"
    assert answer['bad_issues'][0]['id'] == 2


def test_issue_to_number_of_paragraphs_spring_kurs_incorrect_and_correct_input(supply_json_to_test_num_of_paragraph_kurs_spring_incorrect_and_correct_input):
    """Test issue to correct paragraphs spring kurs with incorrect and correct input"""
    answer = supply_json_to_test_num_of_paragraph_kurs_spring_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == \
           "Not enough paragrpah in issue description: Основные вопросы и задачи, подлежащие разработке при выполнении курсовой " \
           "работы весеннего семестра in issue: Сопрано: если ли жизнь после докера You should have at least 2"
    assert answer['bad_issues'][0]['id'] == 3
    assert str(answer['bad_issues'][1]['error']) == \
           "Not enough paragrpah in issue description: Основные вопросы и задачи, подлежащие разработке при выполнении курсовой " \
           "работы весеннего семестра in issue: Петров: модель земли под коралловым рифом You should have at least 2"
    assert answer['bad_issues'][1]['id'] == 4


def test_issue_to_number_of_paragraphs_spring_kurs_correct_input(supply_json_to_test_num_of_paragraph_kurs_spring_correct_input):
    """Test issue to correct paragraphs spring kurs with correct input"""
    answer = supply_json_to_test_num_of_paragraph_kurs_spring_correct_input
    assert answer['bad_issues'] == []


def test_issue_to_number_of_paragraphs_diplom_incorrect_input(supply_json_to_test_num_of_paragraph_diplom_incorrect_input):
    """Test issue to correct paragraphs diplom with incorrect input"""
    answer = supply_json_to_test_num_of_paragraph_diplom_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == \
           "Not enough paragrpah in issue description: Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы " \
           "in issue Сопрано: если ли жизнь после докера You should have at least 5"
    assert answer['bad_issues'][0]['id'] == 6


def test_issue_to_number_of_paragraphs_diplom_incorrect_and_correct_input(supply_json_to_test_num_of_paragraph_diplom_incorrect_and_correct_input):
    """Test issue to correct paragraphs diplom with incorrect and correct input"""
    answer = supply_json_to_test_num_of_paragraph_diplom_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == \
           "Not enough paragrpah in issue description: Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы " \
           "in issue иванов: модель кактуса You should have at least 5"
    assert answer['bad_issues'][0]['id'] == 7
    assert str(answer['bad_issues'][1]['error']) == \
           "Not enough paragrpah in issue description: Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы " \
           "in issue Сопрано: если ли жизнь после докера You should have at least 5"
    assert answer['bad_issues'][1]['id'] == 8


def test_issue_to_number_of_paragraphs_diplom_correct_input(supply_json_to_test_num_of_paragraph_diplom_correct_input):
    """Test issue to correct paragraphs diplom with correct input"""
    answer = supply_json_to_test_num_of_paragraph_diplom_correct_input
    assert answer['bad_issues'] == []
