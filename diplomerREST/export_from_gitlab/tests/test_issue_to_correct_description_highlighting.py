# pylint: disable=C0301
"""test to correct highlighting description field"""


def test_issue_to_incorrect_input_without_stars_in_beginning(supply_json_to_test_correct_stars_incorrect_input):
    """Test issue to correct highlighting in descriptiont"""
    answer = supply_json_to_test_correct_stars_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got ** in description field: Петров: модель земли под коралловым рифом " \
                                                    "You should fill this field like this **Тема работы:** Программное средство " \
                                                    "формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 2


def test_issue_to_incorrect_input_without_stars_in_the_end(supply_json_to_test_correct_stars_incorrect_input_in_the_end):
    """Test issue to correct highlighting in description"""
    answer = supply_json_to_test_correct_stars_incorrect_input_in_the_end
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got ** in description field: Петров: модель  под " \
                                                    "коралловым риф You should fill this field like this **Тема работы:** " \
                                                    "Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 2


def test_issue_to_incorrect_input_without_stars(supply_json_to_test_correct_stars_incorrect_input_without_highlighting):
    """Test issue to correct highlighting in description"""
    answer = supply_json_to_test_correct_stars_incorrect_input_without_highlighting
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got ** in description field: Петров: модель  под " \
                                                    "риф You should fill this field like this **Тема работы:** " \
                                                    "Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 2
