# pylint: disable=C0301,C0121
"""Test to description existance"""


def test_to_description_existance_incorrect_input(supply_json_to_test_description_existance_incorrect_input):
    """Test description existance with incorrect input"""
    answer = supply_json_to_test_description_existance_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t write anything in descrption issue: иванов: модель кактуса"
    assert answer['bad_issues'][0]['id'] == 3


def test_to_description_existance_correct_input(supply_json_to_test_description_existance_correct_input):
    """Test description existance with correct input"""
    answer = supply_json_to_test_description_existance_correct_input
    assert answer['bad_issues'] == []


def test_to_description_existance_incorrect_and_correct_input(supply_json_to_test_description_existance_incorrect_and_correct_input):
    """Test description existance with incorrect and correct input"""
    answer = supply_json_to_test_description_existance_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t write anything in descrption issue: Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 3
    assert str(answer['bad_issues'][1]['error']) == "Probably you haven`t write anything in descrption issue: иванов: модель кактуса"
    assert answer['bad_issues'][1]['id'] == 4
