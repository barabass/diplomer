# pylint: disable=C0301
"""Test to correct symbols in issue"""


def test_issue_to_correct_symbols_description_incorrect_input(supply_json_to_test_correct_symbols_in_description_incorrect_input):
    """Test description with incorrect symbols"""
    answer = supply_json_to_test_correct_symbols_in_description_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == \
           "Probably you have incorrect symbols in description: Тема курсовой работы семестра or kjh,замените_Т_Е_М_У in issue:" \
           " Петров: модель земли под коралловым рифом You should fill this field like this **Тема работы:** Программное средство формирования" \
           " расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 2


def test_issue_to_correct_symbols_description_incorrect_and_correct_input(supply_json_to_test_correct_symbols_in_description_incorrect_and_correct_input):
    """Test description with incorrect and correct symbols"""
    answer = supply_json_to_test_correct_symbols_in_description_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == \
           "Probably you have incorrect symbols in description: Тема курсовой работы семестра or bnmlkknзамените_Т_Е_М_У in issue:" \
           " Сопрано: если ли жизнь после докера You should fill this field like this **Тема работы:** Программное средство формирования" \
           " расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 3
    assert str(answer['bad_issues'][1]['error']) == \
           "Probably you have incorrect symbols in description: Тема курсовой работы семестра or kjh,замените_Т_Е_М_У in issue:" \
           " Петров: модель земли под коралловым рифом You should fill this field like this **Тема работы:** Программное средство формирования" \
           " расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][1]['id'] == 2


def test_issue_to_correct_symbols_description_correct_input(supply_json_to_test_correct_symbols_in_description_correct_input):
    """Test description with correct symbols"""
    answer = supply_json_to_test_correct_symbols_in_description_correct_input
    assert answer['bad_issues'] == []
