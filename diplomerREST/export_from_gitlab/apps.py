"""Add app"""
from django.apps import AppConfig


class ExportFromGitlabConfig(AppConfig):
    """Add app"""
    name = 'export_from_gitlab'
