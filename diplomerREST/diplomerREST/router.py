#pylint: disable=C0103
"""Router to api """
from rest_framework import routers
from export_from_gitlab.viewsets import ValidIssue, InvalidIssue


router = routers.DefaultRouter()
router.register("issue", ValidIssue, basename='issue')
router.register("check_correct", InvalidIssue, basename='check_correct')
urlpatterns = router.urls
